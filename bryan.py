#!/usr/bin/env python
# Written by Lewin Boehnke (lboehnke@physnet.uni-hamburg.de), 2009-2015, consulted by Hugo Strand, 2015
"""
Bryan's maxent analytical continuation algorithm

Usage:
  bryan -w <frequencies> [options] [(-m <file> | -M <m1,m2>)] <datafile>

Options:
  -w --frequencies=<#omega>        Number of real frequency points
  -W --frequencyrange=<omega_max>  Frequncy range. Symmetric for -s F, positiv for -s X. Ignored if modelfile is provided [default: 20.0]
  -S --statistics=<(F|X)>          Fermionic or real bosonic continuation [default: F]
  -m --modelfile=<file>            Provide a default model
  -M --moments=<m1,m2>             Moment list for default model. m0 is fixed to 1.0
  -s --sigma=<sigma>               Standard deviation of the input data. If not provided, it is assumed to be an additional column in the input data file
  -o --outfile=<file>              Result is written to <file> instead of <datafile>_dos.dat
  -g --gnuplot                     Enable live gnuplot view of algorithm (recommended)
  -h --help                        Show this message and exit
"""

try:
    import Gnuplot
    from Gnuplot import Data
    gnuplot=True
except ImportError:
    gnuplot=False
import numpy
import warnings
import itertools
from scipy.optimize import leastsq
from scipy import interpolate
import sys
from math import sqrt, pi
from numpy import dot, sum, exp
import time
from docopt import docopt

class Bryan:
    def __init__(self, tauMesh, omegaMesh, Beta, model, kernel='fermionic',norm=1.0):
        self.tauMesh=tauMesh
        self.omegaMesh=omegaMesh
        self.Beta=Beta*1.0
        self.deltaTau=tauMesh[1]-tauMesh[0]
        self.deltaOmega=omegaMesh[1]-omegaMesh[0]
        if kernel=='fermionic':
            self.K=self.deltaOmega*numpy.array([[exp((Beta/2-tau)*omega)/(exp(Beta/2*omega)+exp(-Beta/2*omega)) for omega in omegaMesh] for tau in tauMesh], numpy.float64)
        elif kernel=='realbosonic':
            self.K=self.deltaOmega*numpy.array([[(numpy.cosh((Beta/2-tau)*omega)*(omega/numpy.sinh(Beta/2*omega) if abs(omega)>1e-8 else 2.0/Beta)*norm/2.0) if Beta*omega<700.0 else (numpy.exp(-tau*omega)+exp(-(Beta-tau)*omega))*omega*norm/2.0 for omega in omegaMesh] for tau in tauMesh], numpy.float64)
        print "Calculating SVD"
        (V,Sigma,Ut)=numpy.linalg.svd(self.K)
        print "Done"
        self.model=numpy.array(model,numpy.float64)
        self.oneovermodel=numpy.where(model>0.0,1.0/model,0.0)
        good=[i for i in range (Sigma.shape[0]) if Sigma[i]>Sigma[-1]*10]
        self.s=len(good)
        self.TSigma=Sigma[good]
        self.TU=Ut.transpose()[:,good]
        self.TVt=V.transpose()[good,:]
        print "Dimension of kernel:%s; Dimension of singular space:%s"%(self.K.shape, self.s)

    def initG(self,G,sigmapm2):
        self.G=numpy.array(G,numpy.float64)
        self.sigmapm2=numpy.array(sigmapm2,numpy.float64)
        self.M=dot(dot(numpy.diag(self.TSigma),dot(dot(self.TVt,numpy.diag(self.sigmapm2)),self.TVt.transpose())),numpy.diag(self.TSigma))

    def maxent(self,alpha,initA=None):
        if initA==None:
            initA=numpy.array([1.0/(sqrt(2*pi)*0.5)*exp(-((x)/0.5)**2*0.5)+1e-3 for x in self.omegaMesh],numpy.float64)
        print "Calculating maxent with alpha=%s"%alpha
        A=numpy.copy(initA)
        A+=1e-8
        u=dot(self.TU.transpose(),numpy.log(A*self.oneovermodel+1e-8))
        mu=1e-2
        while True:
            G_=dot(self.TVt.transpose(),dot(numpy.diag(self.TSigma),dot(self.TU.transpose(),A)))
            dG=G_+self.G
            g=dot(numpy.diag(self.TSigma),dot(self.TVt,dG*self.sigmapm2))
            K=dot(dot(self.TU.transpose(),numpy.diag(A)),self.TU)
            MK=dot(self.M,K)
            du=numpy.linalg.solve((alpha)*numpy.eye(MK.shape[0])+MK, -alpha*u-g)
            mdu=dot(dot(du,K),du)
            if mdu<1e-7: break # there is an alternate criterion in Bryan's paper
            if mdu>1:
                du=numpy.linalg.solve((alpha+mu)*numpy.eye(MK.shape[0])+MK, -alpha*u-g)
                mdu=dot(dot(du,K),du)
                while (abs(mdu-1)>1e-4):
                    mu*=1+(mdu-1)
                    du=numpy.linalg.solve((alpha+mu)*numpy.eye(MK.shape[0])+MK, -alpha*u-g)
                    mdu=dot(dot(du,K),du)
            u+=du
            A=self.model*numpy.exp(numpy.dot(self.TU,u))
        lam=numpy.linalg.eigvalsh(MK)
        G=numpy.log(numpy.where(A>1e-6,A*(self.oneovermodel+1e-8),1e-6*self.oneovermodel))
        S=-self.deltaOmega*sum((A*G)[G>-numpy.inf])
        L=0.5*sum((G_+self.G)**2*self.deltaTau*self.sigmapm2)
        print "Q=%s"%(alpha*S-L)
        return (A,S,L,lam,-G_)

    def approxMax(self,aMax=1.0):
        AMax=self.model.copy()
        while True:
            (AMax,S,L,lam,_)=self.maxent(aMax,AMax)
            tmp=sum(lam/(aMax+lam))/(-2*aMax*S)
            if abs(tmp-1)<1e-4: break
            print tmp
            aMax*=(tmp-1)*1.0+1
        return(aMax,AMax,S,L,lam)

    def screen (self, log_a, step, initA, functor):
        A=numpy.copy(initA)
        while True:
            log_a+=step
            a=numpy.exp(log_a)
            (A,S,L,lam,G)=self.maxent(a,A)
            if not (functor(a,A,self.model,S,L,lam,G)):
                break
    
class MeasureFunctor:
    if gnuplot:
        plot=Gnuplot.Gnuplot()
        plot2=Gnuplot.Gnuplot()
        plot3=Gnuplot.Gnuplot()
    def __init__(self, tauMesh, omegaMesh, thresh, a, A, S, L, lam,G):
        self.G=G
        self.omegaMesh=omegaMesh
        self.tauMesh=tauMesh
        self.thresh=thresh
        logP=0.5*numpy.sum(numpy.log(a/(a+lam)))+a*S-L
        self.logAlpha=[numpy.log(a)]
        self.logPrAlpha=[logP]
        self.sumPrAlpha=numpy.exp(logP)
        self.sumPrAlphaA=numpy.exp(logP)*numpy.copy(A)

    def add (self, a, A, model, S, L, lam, G):
        logP=0.5*numpy.sum(numpy.log(a/(a+lam)))+a*S-L
        self.logAlpha.append(numpy.log(a))
        self.logPrAlpha.append(logP)
        self.sumPrAlpha+=numpy.exp(logP)
        self.sumPrAlphaA+=numpy.exp(logP)*A
        if gnuplot:
            self.plot.plot(Data(self.logAlpha,self.logPrAlpha, with_="l"), Data(self.logAlpha,numpy.exp(self.logPrAlpha), with_="l", axes="x1y2"))
            self.plot2.plot(Data(self.omegaMesh, A, with_="lp"), Data(self.omegaMesh, model, with_='l ls 3',inline=True))
            self.plot3.plot(Data(self.tauMesh,self.G,with_="p"),Data(self.tauMesh,G,with_="l ls 3",inline=True))
        print "logP=%s, thresh=%s"%(logP, self.thresh)
        return (logP>self.thresh)

    def finalize (self):
        return self.sumPrAlphaA/self.sumPrAlpha

if __name__=="__main__":
    arguments=docopt(__doc__)
    if not(arguments['--gnuplot']):
        gnuplot=False

    ########### READ FILE ###########
    if arguments['--statistics']=='X':
        Ns=1
        Ww=numpy.loadtxt(arguments['<datafile>'])[:,:2]
        Beta=2*numpy.pi/Ww[1,0]
        p,c,d=leastsq(lambda p,m,d: [sum([(pi/((1j*mi)**(2*i))).real for i,pi in enumerate(p)])-di for mi,di in itertools.izip(m,d)],[1,2,4],args=(Ww[-50:,0],Ww[-50:,1]))[0]
        Ww[:,1]-=p
        tauMesh=numpy.linspace(0,Beta,Ww.shape[0]/2-1)
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            oneoverWwmesh=numpy.where(Ww[:,0]>0.0,1/Ww[:,0],0.0)
        G=(numpy.array([numpy.sum(numpy.cos(Ww[:,0]*t)*(Ww[:,1]+c*oneoverWwmesh**2-d*oneoverWwmesh**4)) for t in tauMesh])*2.0-Ww[0,1])/Beta+c*(-1.0/12*Beta+1.0/2*tauMesh-tauMesh**2/2.0/Beta)+d*(Beta**3-30*Beta*tauMesh**2+60*tauMesh**3-30./Beta*tauMesh**4)/720.
        G=G.reshape(G.shape[0],1)
        if arguments["--sigma"]:
            sigmapm2=G*0.0+1.0/float(arguments['--sigma'])**2
        else:
            print >> sys.stderr, "--sigma has to be specified for -S X"
            sys.exit(1)

        if arguments['--moments']:
            print >> sys.stderr, "--moments not implemented for -S X"
            sys.exit(1)
        omegaMesh=numpy.linspace(0,float(arguments['--frequencyrange']),int(arguments['--frequencies']))
        if arguments['--modelfile']:
            origomegaMesh=numpy.loadtxt(arguments['--modelfile'])[:,0]
            with warnings.catch_warnings():
                warnings.simplefilter("ignore")
            origmodel=-numpy.loadtxt(arguments['--modelfile'])[0:,-1]
            f=interpolate.interp1d(origomegaMesh,origmodel,kind='linear',fill_value=1e-15,bounds_error=False)
            with warnings.catch_warnings():
                warnings.simplefilter("ignore")
                oneoveromegaMesh=numpy.where(omegaMesh==0.0,0.0,1/omegaMesh)
            model=-f(omegaMesh)*oneoveromegaMesh*2.0/numpy.pi/Ww[0,1]+1e-8
        else:
            model=omegaMesh*0.0+1.0
            
        model/=numpy.trapz(model,omegaMesh)
        B=Bryan(tauMesh,omegaMesh,Beta,model,kernel='realbosonic',norm=-Ww[0,1])
    elif arguments['--statistics']=='F':
        omegaMesh=numpy.linspace(-float(arguments["--frequencyrange"])/2.0,float(arguments["--frequencyrange"])/2.0,int(arguments["--frequencies"]))
        Gtau=numpy.loadtxt(arguments["<datafile>"],numpy.float128)
        tauMesh=Gtau[:,0]
        Beta=tauMesh[0]+tauMesh[-1]
        
        if arguments["--sigma"]:
            Ns=Gtau.shape[1]-1
            sigmaFromFile=False
        else:
            Ns=(Gtau.shape[1]-1)/2
            sigmapm2=[[] for i in range(Ns)]
            sigmaFromFile=True
        G=Gtau[:,1::(2 if sigmaFromFile else 1)]
        if sigmaFromFile:
            sigmapm2=1./Gtau[:,2::2]**2
        else:
            sigmapm2=G*0.0+1.0/float(arguments['--sigma'])**2

        if arguments['--modelfile']:
            print >> sys.stderr, "modelfile not implemented for -S F"
            sys.exit(1)
        if arguments['--moments']:
            m=numpy.array(arguments['--moments'].split(','),numpy.float)
            model=numpy.exp(-(omegaMesh-m[0])**2/2.0/m[1]**2)/sqrt(2*numpy.pi*m[1]**2)+1e-8
            print "using moments %s"%m
        else:
            model=numpy.ones(omegaMesh.shape, numpy.float64)
        model/=numpy.trapz(model,omegaMesh)

        print "Done"

        ######### MAXENTING #############
        
        B=Bryan(tauMesh, omegaMesh, Beta, model)
    else:
        print >> sys.stderr, "only real bosonic (X) and fermionic (F) continuation for now"
        sys.exit(1)
        
    outputA=[]
    print "shape(G)=%s, shape(sigmapm2=%s)"%(G.shape,sigmapm2.shape)
    for i in range(G.shape[1]):
        B.initG(G[:,i], sigmapm2[:,i])
        (aMax,AMax,S,L,lam)=B.approxMax()
        F=MeasureFunctor(tauMesh,omegaMesh,(0.5*numpy.sum(numpy.log(aMax/(aMax+lam)))+aMax*S-L)-3, aMax, AMax, S, L, lam,G[:,i])
        if gnuplot:
            F.plot("set arrow 1 from %s, graph 0 to %s, graph 1 nohead"%(numpy.log(aMax), numpy.log(aMax)))


        B.screen(numpy.log(aMax), 0.1, AMax, F.add)
        B.screen(numpy.log(aMax), -0.1, AMax, F.add)
        ABryan=F.finalize()
        if gnuplot:
            F.plot2.plot(Data(omegaMesh,ABryan, with_="l", title="Bryan's MaxEnt"), Data([0],[1e400/1e-99]), Data(omegaMesh, AMax, with_="l", title="Classic MaxEnt", inline=True))
        if arguments['--statistics']=='F':
            outputA.append(numpy.copy(ABryan))
        else:
            outputA.append(ABryan*B.omegaMesh*numpy.pi/2.0*Ww[0,1])
        if gnuplot:
            print "Press return to go on"
            sys.stdin.readline()

    ####### WRTINTING FILE #########

    print "writing output"
    outfile=arguments['--outfile'] if arguments['--outfile'] else arguments['<datafile>']+'_dos.dat'
    numpy.savetxt(outfile,numpy.hstack([B.omegaMesh.reshape(B.omegaMesh.shape[0],1),numpy.array(outputA).transpose()]))
    
    # GtauRepFile=open(sys.argv[1]+"_reproduced.dat", 'w')
    # for i in range(len(tauMesh)):
    #     s="%s  "%tauMesh[i]
    #     for j in range (Ns):
    #         s+="%s  "%(-numpy.dot(B.K[i,:],outputA[j]))
    #     s+="\n"
    #     GtauRepFile.write(s)
    # GtauRepFile.close()
    print "done"
